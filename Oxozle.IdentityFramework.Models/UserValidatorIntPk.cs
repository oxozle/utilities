﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace Oxozle.IdentityFramework.Models
{
    /// <summary>
    /// https://gist.github.com/blachniet/7394005
    /// </summary>
    public class UserValidatorIntPk : UserValidator<ApplicationUser, int>
    {
        private UserManager<ApplicationUser, int> _manager;


        public UserValidatorIntPk(UserManager<ApplicationUser, int> manager)
            : base(manager)
        {
            _manager = manager;
            AllowOnlyAlphanumericUserNames = false;
            //RequireUniqueEmail = false;

        }

        //private async Task ValidateUserName(ApplicationUser user, List<string> errors)
        //{
        //    return;
        //    //if (string.IsNullOrWhiteSpace(user.UserName))
        //    //{
        //    //    errors.Add(String.Format(CultureInfo.CurrentCulture, Resources.PropertyTooShort, "Name"));
        //    //}
        //    //else if (AllowOnlyAlphanumericUserNames && !Regex.IsMatch(user.UserName, @"^[A-Za-z0-9@_\.]+$"))
        //    //{
        //    //    // If any characters are not letters or digits, its an illegal user name
        //    //    errors.Add(String.Format(CultureInfo.CurrentCulture, Resources.InvalidUserName, user.UserName));
        //    //}
        //    //else
        //    //{
        //    //    //var owner = await Manager.FindByNameAsync(user.UserName);
        //    //    //if (owner != null && !EqualityComparer<string>.Default.Equals(owner.Id, user.Id))
        //    //    //{
        //    //    //    errors.Add(String.Format(CultureInfo.CurrentCulture, Resources.DuplicateName, user.UserName));
        //    //    //}
        //    //}
        //}


        public async override Task<IdentityResult> ValidateAsync(ApplicationUser item)
        {
            var errors = new List<string>();
            if (!IsValid(item.Email))
                errors.Add("Укажите верный email");

            if (_manager != null && !string.IsNullOrEmpty(item.Email))
            {
                var otherAccount = await _manager.FindByEmailAsync(item.Email);
                if (otherAccount != null && otherAccount.Id != item.Id)
                    errors.Add("Выберите другой email. Аккаунт с таким email уже существует.");
            }

            return errors.Any()
                ? IdentityResult.Failed(errors.ToArray())
                : IdentityResult.Success;
        }

        public bool IsValid(string emailaddress)
        {
            if (string.IsNullOrEmpty(emailaddress))
                return true;

            try
            {
                MailAddress m = new MailAddress(emailaddress);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
            catch
            {
                return false;
            }
        }
    }
}
