﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace Oxozle.IdentityFramework.Models
{
    public class AppUserClaimsIdentityFactory : ClaimsIdentityFactory<ApplicationUser, int>
    {
        public override Task<ClaimsIdentity> CreateAsync(UserManager<ApplicationUser, int> manager, ApplicationUser user, string authenticationType)
        {
            var userIdentity = base.CreateAsync(manager, user, authenticationType);
            //var identity = new ApplicationIdentity(user, userIdentity);

            // Add custom user claims here
            return userIdentity;
        }

        //public override async Task<ClaimsIdentity> CreateAsync(
        //    UserManager<ApplicationUser> manager,
        //    ApplicationUser user,
        //    string authenticationType)
        //{
        //    var identity = await base.CreateAsync(manager, user, authenticationType);
        //    //identity.AddClaim(new Claim(ClaimTypes.Country, user.Country));

        //    return identity;
        //}
    }

}
