﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using Oxozle.Utilities;

namespace Oxozle.IdentityFramework.Models
{
    public class ApplicationIdentity : ClaimsIdentity
    {
        public ApplicationIdentity(ApplicationUser user, ClaimsIdentity identity) : base(identity)
        {
            Name = user.UserName;
            Email = user.Email;
            Id = user.Id;
        }

        public virtual new string Name { get; private set; }

        public string Email { get; private set; }
        public virtual new int Id { get; private set; }
    }

    public class ApplicationPrincipal : ClaimsPrincipal
    {
        public ApplicationPrincipal(ApplicationIdentity identity) : base(identity)
        {
        }

        public override bool IsInRole(string role)
        {
            string[] roles = role.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var singleRole in roles)
            {
                if (base.IsInRole(singleRole.Trims()))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
