﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;

namespace Oxozle.IdentityFramework.Models
{
    public class ApplicationSignInManager : SignInManager<ApplicationUser, int>
    {
        public ApplicationSignInManager(UserManager<ApplicationUser, int> userManager, IAuthenticationManager authenticationManager) : base(userManager, authenticationManager)
        {
        }

        public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options,
         IOwinContext context)
        {
            ApplicationSignInManager manager =
                new ApplicationSignInManager(
                    new ApplicationUserManager(new UserStoreIntPk(context.Get<ApplicationDbContext>())),
                    context.Authentication);


            return manager;
        }

        public ApplicationIdentity CreateApplicationIdentity(ApplicationUser user)
        {
            var claimsIdentity = this.CreateUserIdentity(user);
            var identity = new ApplicationIdentity(user, claimsIdentity);

            return identity;
        }
    }
}
