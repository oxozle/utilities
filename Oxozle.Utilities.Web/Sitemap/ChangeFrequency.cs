﻿// *********************************************************************************
// Date: 	30.06.2014
// Project:	Oxozle.Utilities.Web.ChangeFrequency.cs
//  
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// *********************************************************************************

namespace Oxozle.Utilities.Web.Sitemap
{
    public class ChangeFrequency
    {
        #region Static Properties

        public static ChangeFrequency Always
        {
            get { return new ChangeFrequency("always"); }
        }

        public static ChangeFrequency Hourly
        {
            get { return new ChangeFrequency("hourly"); }
        }

        public static ChangeFrequency Daily
        {
            get { return new ChangeFrequency("daily"); }
        }

        public static ChangeFrequency Weekly
        {
            get { return new ChangeFrequency("weekly"); }
        }

        public static ChangeFrequency Monthly
        {
            get { return new ChangeFrequency("monthly"); }
        }

        public static ChangeFrequency Yearly
        {
            get { return new ChangeFrequency("yearly"); }
        }

        public static ChangeFrequency Never
        {
            get { return new ChangeFrequency("never"); }
        }

        #endregion

        #region Properties

        private readonly string _value;

        public string Value
        {
            get { return _value; }
        }

        #endregion

        #region Constructors

        public ChangeFrequency(string value)
        {
            _value = value;
        }

        #endregion
    }
}