﻿using System.Configuration;

namespace Oxozle.Utilities.Web.WeblogPing.Configuration
{
    public class WeblogConfiguration : ConfigurationSection
    {
        private static WeblogConfiguration _configuration = ConfigurationManager.GetSection("WeblogConfiguration") as WeblogConfiguration;

        public static WeblogConfiguration Settings
        {
            get
            {
                return _configuration;
            }
        }

        [
ConfigurationProperty("pings", IsDefaultCollection = false),
ConfigurationCollection(typeof(WeblogCollection), AddItemName = "addPing", ClearItemsName = "clearPings", RemoveItemName = "removePing")
]
        public WeblogCollection Pings
        {
            get { return this["pings"] as WeblogCollection; }
        }
    }
}
