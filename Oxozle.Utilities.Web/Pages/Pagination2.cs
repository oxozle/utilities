﻿using System.Text;
using System.Web.Mvc;

namespace Oxozle.Utilities.Web.Pages
{
    public static class Pagination2
    {
        public static MvcHtmlString PageLinks(int currentPage, int totalPages, string urlBase)
        {
            StringBuilder builder = new StringBuilder();

            //Prev
            var prevBuilder = new TagBuilder("a");
            prevBuilder.InnerHtml = "«";
            if (currentPage == 1)
            {
                prevBuilder.MergeAttribute("href", "#");
                builder.AppendLine("<li class=\"active\">" + prevBuilder.ToString() + "</li>");
            }
            else
            {
                prevBuilder.MergeAttribute("href", string.Format(urlBase,currentPage-1));
                builder.AppendLine("<li>" + prevBuilder.ToString() + "</li>");
            }
            //По порядку
            for (int i = 1; i <= totalPages; i++)
            {
                //Условие что выводим только необходимые номера
                if (((i <= 3) || (i > (totalPages - 3))) || ((i > (currentPage - 2)) && (i < (currentPage + 2))))
                {
                    var subBuilder = new TagBuilder("a");
                    subBuilder.InnerHtml = i.ToString();
                    if (i == currentPage)
                    {
                        subBuilder.MergeAttribute("href", "#");
                        builder.AppendLine("<li class=\"active\">" + subBuilder.ToString() + "</li>");
                    }
                    else
                    {
                        subBuilder.MergeAttribute("href", string.Format(urlBase, i));
                        builder.AppendLine("<li>" + subBuilder.ToString() + "</li>");
                    }
                }
                else if ((i == 4) && (currentPage > 5))
                {
                    //Троеточие первое
                    builder.AppendLine("<li class=\"disabled\"> <a href=\"#\">...</a> </li>");
                }
                else if ((i == (totalPages - 3)) && (currentPage < (totalPages - 4)))
                {
                    //Троеточие второе
                    builder.AppendLine("<li class=\"disabled\"> <a href=\"#\">...</a> </li>");
                }
            }
            //Next
            var nextBuilder = new TagBuilder("a");
            nextBuilder.InnerHtml = "»";
            if (currentPage == totalPages)
            {
                nextBuilder.MergeAttribute("href", "#");
                builder.AppendLine("<li class=\"active\">" + nextBuilder.ToString() + "</li>");
            }
            else
            {
                nextBuilder.MergeAttribute("href", string.Format(urlBase, currentPage + 1));
                builder.AppendLine("<li>" + nextBuilder.ToString() + "</li>");
            }
            return new MvcHtmlString("<ul class=\"pagination\">" + builder.ToString() + "</ul>");
        }

        public static MvcHtmlString PageLinksWithoutLI(int currentPage, int totalPages, string urlTemplate)
        {
            StringBuilder builder = new StringBuilder();


            //По порядку
            for (int i = 1; i <= totalPages; i++)
            {
                //Условие что выводим только необходимые номера
                if (((i <= 3) || (i > (totalPages - 3))) || ((i > (currentPage - 2)) && (i < (currentPage + 2))))
                {
                    var subBuilder = new TagBuilder("a");
                    subBuilder.InnerHtml = i.ToString();
                    if (i == currentPage)
                    {
                        builder.AppendLine("<span class=\"current\">" + i + "</span>");
                    }
                    else
                    {
                        subBuilder.MergeAttribute("href", string.Format(urlTemplate, i));
                        builder.AppendLine(subBuilder.ToString());
                    }
                }
                else if ((i == 4) && (currentPage > 5))
                {
                    //Троеточие первое
                    builder.AppendLine("<li class=\"disabled\"> <a href=\"#\">...</a> </li>");
                }
                else if ((i == (totalPages - 3)) && (currentPage < (totalPages - 4)))
                {
                    //Троеточие второе
                    builder.AppendLine("<li class=\"disabled\"> <a href=\"#\">...</a> </li>");
                }
            }

            return new MvcHtmlString(builder.ToString());
        }

        public static MvcHtmlString PrevNextLinksWithPageIDTemplate(int currentPage, int totalPages, string urlTemplate)
        {
            StringBuilder builder = new StringBuilder();

            if (currentPage < totalPages)
            {
                //Prev
                var prevBuilder = new TagBuilder("a");
                prevBuilder.AddCssClass("btn");
                prevBuilder.AddCssClass("prev");
                prevBuilder.InnerHtml = "&larr; Старее ";
                prevBuilder.MergeAttribute("href", string.Format(urlTemplate, currentPage + 1));
                builder.AppendLine(prevBuilder.ToString());
            }



            //Next
            var nextBuilder = new TagBuilder("a");
            nextBuilder.InnerHtml = "Новее &rarr;";
            nextBuilder.AddCssClass("btn");
            nextBuilder.AddCssClass("next");

            if (currentPage > 1)
            {
                if (currentPage == 2)
                {
                    nextBuilder.MergeAttribute("href", "/");
                }
                else
                {
                    nextBuilder.MergeAttribute("href", string.Format(urlTemplate, currentPage - 1));
                }
                builder.AppendLine(nextBuilder.ToString());

            }
            return new MvcHtmlString(builder.ToString());
        }
    }
}
