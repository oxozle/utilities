﻿// *********************************************************************************
// Author:	Azarov Dmitriy
// Email:	oxozle@gmail.com
// Date: 	30.06.2014
// Project:	Oxozle.Utilities.Web.OxoDataValidator.cs
//  
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// *********************************************************************************

#region Usings

using System.Web.ModelBinding;

#endregion

namespace Oxozle.Utilities.Web
{
    /// <summary>
    ///   Класс для сбора информации об ошибках
    /// </summary>
    public class OxoDataValidator
    {
        private readonly ModelStateDictionary _modelState;

        public OxoDataValidator()
        {
        }

        public OxoDataValidator(ModelStateDictionary modelstate)
        {
            _modelState = modelstate;
        }

        private void AddModelError(string entry, string value)
        {
            if (_modelState != null)
                _modelState.AddModelError(entry, value);
        }


        public string RequiredString(string value, int maxLength, string fieldName, string errorTitle)
        {
            value = value.Trims();
            if (string.IsNullOrEmpty(value))
            {
                AddModelError(fieldName, errorTitle);
                return string.Empty;
            }

            return MaxLength(value, maxLength, fieldName, errorTitle);
        }

        /// <summary>
        ///   Возвращает число если оно попадает в промежуток или defaultValue если не попадает
        /// </summary>
        /// <param name="testValue"> Число (в строке) на проверку </param>
        /// <param name="min"> Манимум (входит в интервал) </param>
        /// <param name="max"> Максимум (входит в интервал) </param>
        /// <param name="defaultValue"> Значение по умочанию </param>
        public int Between(string testValue, int min, int max, int defaultValue, string fieldName, string fieldValue)
        {
            int value = testValue.GetInteger(defaultValue);

            if (value < min || value > max)
            {
                AddModelError(fieldName, fieldValue);
                return defaultValue;
            }

            return value;
        }

        /// <summary>
        ///   Возвращает число если оно попадает в промежуток или defaultValue если не попадает
        /// </summary>
        /// <param name="testValue"> Число (в строке) на проверку </param>
        /// <param name="min"> Манимум (входит в интервал) </param>
        /// <param name="max"> Максимум (входит в интервал) </param>
        /// <param name="defaultValue"> Значение по умочанию </param>
        public short BetweenShort(string testValue, short min, short max, short defaultValue, string fieldName,
            string fieldValue)
        {
            short value = OxoValidation.GetShort(testValue, defaultValue);

            if (value < min || value > max)
            {
                AddModelError(fieldName, fieldValue);
                return defaultValue;
            }

            return value;
        }

        /// <summary>
        ///   Проверяет поле на длину строки. Показывает ошибку, если в поле есть ошибка. Внутри вызывает Trims.
        /// </summary>
        /// <param name="testValue"> Текст для проверки </param>
        /// <param name="maxLength"> Максимальная длина </param>
        /// <param name="fieldName"> Имя поля с ошибкой </param>
        /// <param name="error"> Текст ошибки </param>
        /// <returns> Поле обрезанное по максимальной длине </returns>
        public string MaxLength(string testValue, int maxLength, string fieldName, string error)
        {
            string value = testValue.Trims();

            if (string.IsNullOrEmpty(value) || value.Length <= maxLength)
                return value;

            AddModelError(fieldName, error);
            return value.Substring(0, maxLength);
        }

        /// <summary>
        ///   Проверяет текст на максимальную длину. Возвращает обрезанное поле, если текст длиннее максимального. Внутри вызывает Trims.
        /// </summary>
        /// <param name="testValue"> Текст </param>
        /// <param name="maxLength"> Максимальная длина </param>
        /// <returns> Обрезанные или обычный текст </returns>
        public string MaxLengthSafe(string testValue, int maxLength)
        {
            if (string.IsNullOrEmpty(testValue))
                return null;

            string value = testValue.Trims();

            if (value.Length <= maxLength)
                return value;

            return value.Substring(0, maxLength);
        }


        /// <summary>
        ///   Возвращает null если число равно нулю или само число
        /// </summary>
        public int? NotZero(int value)
        {
            if (value == 0)
                return null;
            return value;
        }

        /// <summary>
        ///   Возвращает null если число равно нулю или само число
        /// </summary>
        public int? NotZero(string value)
        {
            return NotZero(value.GetInteger(0));
        }
    }
}