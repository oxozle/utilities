﻿#region Unigs

using System;
using System.Collections.Generic;
using System.Linq;

#endregion

namespace Oxozle.Utilities
{
    public static class OxoTypeHelper
    {
        /// <summary>
        ///   Возвращает список из перечисления
        /// </summary>
        public static List<T> GetList<T>()
        {
            return Enum.GetValues(typeof (T)).Cast<T>().ToList();
        }
    }
}