﻿#region Unigs

using System;
using System.Collections.Generic;
using System.Linq;

#endregion

namespace Oxozle.Utilities.Utils
{
    public static class EnumUtils
    {
        public static IEnumerable<T> GetValues<T>()
        {
            return Enum.GetValues(typeof(T)).Cast<T>();
        }
    }
}
