﻿#region Unigs

using System;

#endregion

namespace Oxozle.Utilities.Utils
{
    public static class StaticRandom
    {
        static readonly Random _rand = new Random();
        static readonly object _lock = new object();

        public static int Next()
        {
            lock (_lock)
            {
                return _rand.Next();
            }
        }

        public static int Next(int max)
        {
            lock (_lock)
            {
                return _rand.Next(max);
            }
        }


        public static int Next(int min, int max)
        {
            lock (_lock)
            {
                return _rand.Next(min, max);
            }
        }
    }
}
