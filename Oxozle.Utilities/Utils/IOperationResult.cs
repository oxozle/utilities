﻿#region Unigs

using System.Collections.Generic;

#endregion

namespace Oxozle.Utilities.Utils
{
    public interface IOperationResult
    {
        bool Success { get; }

        List<string> Messages { get; }
    }

    public interface IOperationResult<T>
    {
        bool Success { get; }

        List<string> Messages { get; }

        T Object { get; }
    }

    public sealed class OperationResult<T> : OperationResult, IOperationResult<T>
    {
        static OperationResult()
        {
            OK = new OperationResult<T>("");
        }

        public OperationResult(string message) : base(message)
        {
        }

        public OperationResult(List<string> messages) : base(messages)
        {
        }

        public OperationResult(T obj)
        {
            Object = obj;
        }


        public static OperationResult<T> OK { get; private set; }
        public T Object { get; set; }
    }

    public class OperationResult : IOperationResult
    {
        static OperationResult()
        {
            OK = new OperationResult();
        }

        public OperationResult()
        {
            Messages = new List<string>();
        }

        public OperationResult(List<string> messages)
        {
            Messages = messages;
        }

        public OperationResult(string message)
            : this()
        {
            if (!message.IsEmpty())
                Messages.Add(message);
        }

        public static OperationResult OK { get; private set; }


        public bool Success
        {
            get { return Messages.Count == 0; }
        }

        public List<string> Messages { get; }
    }
}
