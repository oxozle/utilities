﻿#region Unigs

using System.Collections;
using System.Collections.Generic;

#endregion

namespace Oxozle.Utilities
{
    /// <summary>
    /// Helper methods for work with IDrictionary
    /// </summary>
    public static class OxoDictionaryHelper
    {
        /// <summary>
        /// Add value to dicrionary
        /// </summary>
        public static void Add(IDictionary items, string key, object value, bool caseSensitive = false)
        {
            if ((key != null) && (items != null))
            {
                if (!caseSensitive)
                {
                    key = key.ToUpper();
                }
                items[key] = value;
            }
        }

        /// <summary>
        /// Remove all items from dictionary starts with
        /// </summary>
        public static void Clear(IDictionary items, string startsWith)
        {
            if ((startsWith != null) && (items != null))
            {
                startsWith = startsWith.ToUpper();

                List<string> removeItemsKeys = new List<string>();

                IDictionaryEnumerator enumerator = items.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    string item = enumerator.Key.ToString().ToUpper();
                    if (item.StartsWith(startsWith))
                    {
                        removeItemsKeys.Add(item);
                    }
                }

                foreach (string removeKey in removeItemsKeys)
                {
                    Remove(items, removeKey, false);
                }
            }
        }

        /// <summary>
        /// Return true if exists element with key
        /// </summary>
        public static bool Contains(IDictionary items, string key, bool caseSensitive = false)
        {
            if (items == null || key == null)
                return false;


            if (!caseSensitive)
                key = key.ToUpper();

            return items[key] != null;
        }

        /// <summary>
        /// Return dictionary item by key
        /// </summary>
        public static object GetItem(IDictionary items, string key, bool caseSensitive = false)
        {
            if (items == null || key == null)
                return null;

            if (!caseSensitive)
                key = key.ToUpper();

            return items[key];
        }

        /// <summary>
        /// Remove item
        /// </summary>
        public static void Remove(IDictionary items, string key, bool caseSensitive = false)
        {
            if (items != null && key != null)
            {
                if (!caseSensitive)
                    key = key.ToUpper();

                items[key] = null;
            }
        }
    }
}