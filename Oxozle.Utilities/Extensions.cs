﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oxozle.Utilities
{
    public static class Extensions
    {
        public static string HostWithScheme(this Uri uri)
        {
            if (uri.Port == 80 || uri.Port == 443)
            {
                return uri.Scheme + Uri.SchemeDelimiter + uri.Host;
            }
            return uri.Scheme + Uri.SchemeDelimiter + uri.Host + ":" + uri.Port;

        }
    }
}
