﻿#region Unigs

using System;
using System.Reflection;

#endregion

namespace Oxozle.Utilities.Attributes
{
    /// <summary>
    ///   Методы для работы с аттрибутами
    /// </summary>
    public static class AttributesHelper
    {
        /// <summary>
        ///   Русский текст
        /// </summary>
        public static string GetText(this Enum en)
        {
            Type type = en.GetType();
            MemberInfo[] memInfo = type.GetMember(en.ToString());

            if (memInfo != null && memInfo.Length > 0)
            {
                object[] attrs = memInfo[0].GetCustomAttributes(typeof(RuText), false);

                if (attrs != null && attrs.Length > 0)
                    return ((RuText)attrs[0]).Text;
            }

            return en.ToString();
        }

        /// <summary>
        ///   Русский текст
        /// </summary>
        public static string GetGenitiveText(this Enum en)
        {
            Type type = en.GetType();
            MemberInfo[] memInfo = type.GetMember(en.ToString());

            if (memInfo != null && memInfo.Length > 0)
            {
                object[] attrs = memInfo[0].GetCustomAttributes(typeof(RuText), false);

                if (attrs != null && attrs.Length > 0)
                    return ((RuText)attrs[0]).Genitive;
            }

            return en.ToString();
        }
    }
}
