﻿#region Unigs

using System;

#endregion

namespace Oxozle.Utilities.Attributes
{
    /// <summary>
    ///   Для перевода или присваивания полю русского текста
    /// </summary>
    public class RuText : Attribute
    {
        public string Genitive;
        public string Text;

        public RuText(string text)
        {
            Text = text;
        }

        public RuText(string text, string genitive)
        {
            Text = text;
            Genitive = genitive;
        }
    }
}
