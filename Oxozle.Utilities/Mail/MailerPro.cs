﻿#region Unigs

using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using Elmah;

#endregion

namespace Oxozle.Utilities.Mail
{
    public interface IMailSender
    {
        void Init(MailerProConfig config);

        void Send(string toEmail, string subject, string body, string copy = null, string hiddenCopy = null,
            string[] attachments = null, Stream attachmentStream = null,
            string attachmentStreamFileName = null);

        //void Send(string toEmail, string subject, string body, string copy = null, string hiddenCopy = null,
        // string[] attachments = null);
    }


    public sealed class MailerPro
    {
        /// <summary>
        /// Если установлено, то вся почта идет туда
        /// </summary>
        public static string DebugOnlyTo = null;

        private readonly SmtpClient _smtp;

        static MailerPro()
        {
        }

        public MailerPro(MailerProConfig config)
        {
            FromAddress = new MailAddress(config.FromEmail);

            _smtp = new SmtpClient
            {
                Host = config.Host,
                Port = config.Port,
                EnableSsl = config.EnableSsl,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(config.UserName, config.Password)
            };
        }


        public MailAddress FromAddress { get; }

        public void Send(string toEmail, string subject, string body, string copy = null, string hiddenCopy = null, bool isBodyHtml = true, string[] attachments = null, Stream attachmentStream = null, string attachmentStreamFileName = null)
        {

            if (!DebugOnlyTo.IsEmpty())
            {
                subject = "[ТЕСТОВОЕ СООБЩЕНИЕ] " + subject;
                toEmail = DebugOnlyTo;
                copy = null;
                hiddenCopy = null;
            }

            if (string.IsNullOrEmpty(toEmail))
            {
                return;
            }

            string[] addresses = toEmail.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries);

            foreach (MailAddress toAddress in addresses.Select(address => new MailAddress(address)))
            {
                SendEmail(toAddress, subject, body, copy, hiddenCopy, isBodyHtml, attachments, attachmentStream, attachmentStreamFileName);
            }
        }

        private void SendEmail(MailAddress to, string subject, string body, string copy, string hiddenCopy, bool isHtml, string[] attachments, Stream attachmentStream = null, string attachmentStreamFileName = null)
        {

//#if DEBUG
            //Debug.WriteLine("Send email to " + to.Address);
//#endif

            using (MailMessage message = new MailMessage(FromAddress, to)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = isHtml
            })
            {
                if (!copy.IsEmpty())
                {
                    foreach (string cc in copy.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        if (cc.IsEmpty())
                            continue;
                        message.CC.Add(cc);
                    }
                }

                if (!hiddenCopy.IsEmpty())
                {
                    foreach (string bcc in hiddenCopy.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        if (bcc.IsEmpty())
                            continue;
                        message.Bcc.Add(bcc);
                    }
                }


                if (attachments != null && attachments.Length > 0)
                    foreach (string attachment in attachments)
                        message.Attachments.Add(new Attachment(attachment));

                if (attachmentStream != null && attachmentStream.Length > 0)
                    message.Attachments.Add(new Attachment(attachmentStream, attachmentStreamFileName));

                try
                {
                    _smtp.Send(message);
                }
                catch (Exception exception)
                {
                    var error = new Error(exception);
                    error.ServerVariables.Add("Email Fields", $"{to}: {subject}");
                    ErrorLog.GetDefault(null).Log(error);
                }

            }
        }
    }
}