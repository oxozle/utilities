﻿namespace Oxozle.Utilities.Mail
{
    public class MailerProConfig
    {
        public MailerProConfig(string userName,string password, string host, int port, string fromName, string fromEmail, bool enableSsl)
        {
            FromName = fromName;
            Email = fromEmail;
            Password = password;
            Host = host;
            Port = port;
            UserName = userName;
            EnableSsl = enableSsl;
        }

        public string FromEmail { get { return $"{FromName} <{Email}>"; } }

        public string FromName { get; }

        public string Email { get; }

        public string Password { get; private set; }

        public string Host { get; private set; } = "smtp.yandex.ru";

        public int Port { get; private set; } = 25;

        public string UserName { get; private set; }

        public bool EnableSsl { get; private set; }
    }
}
