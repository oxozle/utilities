﻿#region Unigs

using System.Text.RegularExpressions;

#endregion

namespace Oxozle.Utilities
{
    public static class OxoRegex
    {
        private static RegexOptions _defaultOptions = RegexOptions.Compiled;

        /// <summary>
        ///   Настройки по умолчанию
        /// </summary>
        public static RegexOptions DefaultOptions
        {
            get { return _defaultOptions; }
            set { _defaultOptions = value; }
        }

        /// <summary>
        ///   Возвращает Regex для шаблона
        /// </summary>
        public static Regex CreateRegex(string pattern)
        {
            return CreateRegex(pattern, DefaultOptions);
        }

        /// <summary>
        ///   Возвращает Regex для шаблона
        /// </summary>
        public static Regex CreateRegex(string pattern, RegexOptions options)
        {
            return new Regex(pattern, options);
        }
    }
}