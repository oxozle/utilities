﻿namespace Oxozle.Utilities
{
    public static class DataValidation
    {
        public static bool RequiredString(string value, int maxLength)
        {
            return RequiredString(value, maxLength, 1);
        }

        public static bool RequiredString(string value, int maxLength, int minLength)
        {
            if (string.IsNullOrEmpty(value))
            {
                return false;
            }

            if (value.Length < minLength || value.Length > maxLength)
            {
                return false;
            }

            return true;
        }

        public static bool MaxLength(this string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value))
                return true;

            if (value.Length > maxLength)
                return false;

            return true;
        }


        /// <summary>
        ///   Проверяет текст на максимальную длину. Возвращает обрезанное поле, если текст длиннее максимального. Внутри вызывает Trims.
        /// </summary>
        /// <param name="testValue"> Текст </param>
        /// <param name="maxLength"> Максимальная длина </param>
        /// <returns> Обрезанные или обычный текст </returns>
        public static string MaxLengthSafe(this string testValue, int maxLength)
        {
            if (string.IsNullOrEmpty(testValue))
                return null;

            string value = testValue.Trims();

            if (value.Length <= maxLength)
                return value;

            return value.Substring(0, maxLength);
        }
    }
}