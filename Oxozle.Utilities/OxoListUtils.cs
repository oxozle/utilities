﻿#region Unigs

using System;
using System.Collections.Generic;
using System.Linq;

#endregion

namespace Oxozle.Utilities
{
    public static class OxoListUtils
    {


        /// <summary>
        /// Breaks the list into groups with each group containing no more than the specified group size
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="values">The values.</param>
        /// <param name="groupSize">Size of the group.</param>
        /// <returns></returns>
        public static List<List<T>> SplitList<T>(this IEnumerable<T> values, int groupSize, int? maxCount = null)
        {
            List<List<T>> result = new List<List<T>>();
            // Quick and special scenario
            if (values.Count() <= groupSize)
            {
                result.Add(values.ToList());
            }
            else
            {
                List<T> valueList = values.ToList();
                int startIndex = 0;
                int count = valueList.Count;
                int elementCount = 0;

                while (startIndex < count && (!maxCount.HasValue || (maxCount.HasValue && startIndex < maxCount)))
                {
                    elementCount = (startIndex + groupSize > count) ? count - startIndex : groupSize;
                    result.Add(valueList.GetRange(startIndex, elementCount));
                    startIndex += elementCount;
                }
            }


            return result;
        }

        public static List<List<T>> Split<T>(this List<T> source, int splitNumber)
        {
            return source
                .Select((x, i) => new { Index = i, Value = x })
                .GroupBy(x => x.Index / splitNumber)
                .Select(x => x.Select(v => v.Value).ToList())
                .ToList();
        }

        public static List<List<T>> SplitForListCount<T>(this List<T> source, int needListCount)
        {
            var resultList = new List<List<T>>();
            for (int i = 0; i < needListCount; i++)
                resultList.Add(new List<T>());

            int counter = 0;
            foreach (T sourceItem in source)
            {
                resultList[counter % needListCount].Add(sourceItem);
                counter++;
            }
            return resultList;

            //var splitNumber = (int)Math.Ceiling(source.Count / (double)needListCount);
            //return source
            //    .Select((x, i) => new { Index = i, Value = x })
            //    .GroupBy(x => x.Index / splitNumber)
            //    .Select(x => x.Select(v => v.Value).ToList())
            //    .ToList();

        }
    }
}
