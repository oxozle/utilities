﻿#region Unigs

using System;
using System.Collections.Generic;
using System.Linq;

#endregion

namespace Oxozle.Utilities.Pages
{
    public class PagedList<T> : List<T>, IPagedList
    {
        public PagedList()
        {

        }


        public PagedList(List<T> list, IPagedList dtoList)
        {
            TotalCount = dtoList.TotalCount;
            TotalPages = (int)Math.Ceiling(dtoList.TotalCount / (double)dtoList.PageSize);
            if (TotalPages == 0)
                TotalPages = 1;

            PageSize = dtoList.PageSize;
            CurrentIndex = dtoList.CurrentPage - 1;

            ValidatePage();
            AddRange(list);
        }

        public PagedList(List<T> list, int page, int total, int perPage)
        {
            TotalCount = total;
            TotalPages = (int)Math.Ceiling(total / (double)perPage);
            if (TotalPages == 0)
                TotalPages = 1;

            PageSize = perPage;
            CurrentIndex = page - 1;

            ValidatePage();
            AddRange(list);
        }

        public PagedList(PagedList<T> pagedList)
        {
            TotalCount = pagedList.TotalCount;
            TotalPages = pagedList.TotalPages;
            PageSize = pagedList.PageSize;
            CurrentIndex = pagedList.CurrentIndex;
            AddRange(pagedList);
        }


        public PagedList(IQueryable<T> source, int currentPage, int pageSize)
        {
            int total = source.Count();
            TotalCount = total;
            TotalPages = (int)Math.Ceiling(total / (double)pageSize);
            if (TotalPages == 0)
                TotalPages = 1;


            PageSize = pageSize;
            CurrentIndex = currentPage - 1;

            ValidatePage();


            AddRange(source.Skip(CurrentIndex * PageSize).Take(PageSize).ToList());
        }


        private void ValidatePage()
        {
            if (CurrentIndex < 0)
                CurrentIndex = 0;

            if (CurrentIndex > TotalPages - 1)
                CurrentIndex = TotalPages - 1;
        }

        #region IPagedList Members

        public int TotalPages { get; set; }

        public int TotalCount { get; set; }

        public int CurrentIndex { get; set; }

        public int CurrentPage
        {
            get { return CurrentIndex + 1; }
        }

        public int PageSize { get; set; }

        public bool IsPreviousPage
        {
            get { return (CurrentPage > 1); }
        }

        public bool IsNextPage
        {
            get { return (CurrentPage * PageSize) < TotalCount; }
        }

        #endregion
    }
}
