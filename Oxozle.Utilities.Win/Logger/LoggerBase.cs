﻿#region Unigs

using System;

#endregion

namespace Oxozle.Utilities.Win.Logger
{
    public abstract class LoggerBase : ILogger
    {
        protected string _applicationName;

        protected LoggerBase(string applicationName)
        {
            _applicationName = applicationName;
        }

        //public string LogName { get; private set; }

        public abstract void DebugMessage(string message);

        public abstract void Information(string message);

        public abstract void Warning(string message);


        public abstract void Error(string message);


        public abstract void Error(string message, Exception exception);
    }
}