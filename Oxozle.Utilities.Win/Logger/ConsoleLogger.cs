﻿#region Unigs

using System;
using System.Text;

#endregion

namespace Oxozle.Utilities.Win.Logger
{
    internal static class ConsoleColorHelper
    {
        public static void Color(this string text, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(text);
            Console.ForegroundColor = ConsoleColor.Gray;
        }
    }


    public class ConsoleLogger : LoggerBase
    {
        public ConsoleLogger(string applicationName)
            : base(applicationName)
        {
        }

        public override void DebugMessage(string message)
        {
            message.Color(ConsoleColor.Gray);
        }

        public override void Information(string message)
        {
            message.Color(ConsoleColor.White);
        }

        public override void Warning(string message)
        {
            message.Color(ConsoleColor.Magenta);
        }

        public override void Error(string message)
        {
            message.Color(ConsoleColor.Red);
        }

        public override void Error(string message, Exception exception)
        {
            StringBuilder messageLogBuilder = new StringBuilder();
            messageLogBuilder.AppendFormat("Message: {0}", message);
            messageLogBuilder.AppendLine();
            messageLogBuilder.AppendLine();
            messageLogBuilder.AppendFormat("Error: {0}", exception.Message);
            messageLogBuilder.AppendLine();
            messageLogBuilder.AppendFormat("Stack Trace: {0}", exception.StackTrace);

            Error(messageLogBuilder.ToString());
        }
    }
}