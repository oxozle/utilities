﻿#region Unigs

using System;
using System.Diagnostics;
using System.Management;
using System.Security.Principal;

#endregion

namespace Oxozle.Utilities.Win.System
{
    /// <summary>
    ///   Класс информации о системе. Получает уникальную информацию о системе на которой запскается
    /// </summary>
    public class SystemInfo
    {
        /// <summary>
        ///   Использовать BaseBoardManufacturer (по молчанию false)
        /// </summary>
        public bool UseBaseBoardManufacturer;

        /// <summary>
        ///   Использовать BaseBoardProduct (true)
        /// </summary>
        public bool UseBaseBoardProduct = true;

        ///// <summary>
        ///// Использовать BiosManufacturer (по молчанию false)
        ///// </summary>
        //public bool UseBiosManufacturer;

        /// <summary>
        ///   Использовать BiosVersion (по молчанию true)
        /// </summary>
        public bool UseBiosVersion = true;

        /// <summary>
        ///   Использовать DiskDriveSignature (по молчанию true)
        /// </summary>
        public bool UseDiskDriveSignature = true;

        /// <summary>
        ///   Использовать PhysicalMediaSerialNumber (по молчанию false)
        /// </summary>
        public bool UsePhysicalMediaSerialNumber;

        /// <summary>
        ///   Использовать ProcessorID
        /// </summary>
        public bool UseProcessorID = true;

        /// <summary>
        ///   Использовать VideoControllerCaption (по молчанию false)
        /// </summary>
        public bool UseVideoControllerCaption;

        /// <summary>
        ///   Использовать WindowsSerialNumber (по молчанию false)
        /// </summary>
        public bool UseWindowsSerialNumber;

        /// <summary>
        ///   Взятие хеша от системной информации
        /// </summary>
        /// <param name="info"> Сведения о системе </param>
        public string GetSystemInfoMD5(string softwareName)
        {
            return OxoCrypt.MD5(GetSystemInfo(softwareName));
        }

        /// <summary>
        ///   Получает строку с системной информацией. Обрезает первые 25 символов
        /// </summary>
        /// <param name="SoftwareName"> Имя приложения </param>
        /// <returns> Строка </returns>
        public string GetSystemInfo(string SoftwareName)
        {
            UseWindowsSerialNumber = true;

            try
            {
                WindowsIdentity windowsIdentity = WindowsIdentity.GetCurrent();
                if (windowsIdentity != null)
                    SoftwareName += windowsIdentity.Name;
            }
            catch (Exception exception)
            {
            }

            try
            {
                if (UseProcessorID)
                    SoftwareName += RunQuery("Processor", "ProcessorId");

                if (UseBaseBoardProduct)
                    SoftwareName += RunQuery("BaseBoard", "Product");

                if (UseBaseBoardManufacturer)
                    SoftwareName += RunQuery("BaseBoard", "Manufacturer");

                if (UseDiskDriveSignature)
                    SoftwareName += RunQuery("DiskDrive", "Signature");

                if (UseVideoControllerCaption)
                    SoftwareName += RunQuery("VideoController", "Caption");

                if (UsePhysicalMediaSerialNumber)
                    SoftwareName += RunQuery("PhysicalMedia", "SerialNumber");

                if (UseBiosVersion)
                    SoftwareName += RunQuery("BIOS", "Version");

                if (UseWindowsSerialNumber)
                    SoftwareName += RunQuery("OperatingSystem", "SerialNumber");

                SoftwareName = RemoveUseLess(SoftwareName);

                if (SoftwareName.Length < 25)
                    return GetSystemInfo(SoftwareName);
            }
            catch (Exception exception)
            {
            }


            //return SoftwareName.Substring(0, 25).ToUpper();
            return SoftwareName;
        }

        /// <summary>
        ///   Удалить неиспользуемые символы из строки. Допустимые символы заглавные английские и цифры.
        /// </summary>
        /// <param name="st"> Строка </param>
        /// <returns> Строка без неиспользуемых символов </returns>
        private string RemoveUseLess(string st)
        {
            char ch;
            for (int i = st.Length - 1; i >= 0; i--)
            {
                ch = char.ToUpper(st[i]);

                if ((ch < 'A' || ch > 'Z') &&
                    (ch < '0' || ch > '9'))
                {
                    st = st.Remove(i, 1);
                }
            }
            return st;
        }

        /// <summary>
        ///   Получает информацию из системы в указаной таблице с указанным именем
        /// </summary>
        /// <param name="TableName"> Таблица системной информации </param>
        /// <param name="MethodName"> Параметр для извлечения информации </param>
        /// <returns> Строка информации или пусстая строка </returns>
        private string RunQuery(string TableName, string MethodName)
        {
            ManagementObjectSearcher MOS = new ManagementObjectSearcher("Select * from Win32_" + TableName);
            foreach (ManagementObject MO in MOS.Get())
            {
                try
                {
                    return MO[MethodName].ToString();
                }
                catch (Exception e)
                {
                    Trace.WriteLine(e.Message);
                }
            }
            return "";
        }
    }
}