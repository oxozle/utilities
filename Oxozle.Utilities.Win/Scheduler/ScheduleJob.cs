﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oxozle.Utilities.Win
{
    public abstract class ScheduleJob
    {
        protected ScheduleJob(string taskId)
        {
            TaskId = taskId;
        }

        //protected ScheduleJob(int taskObjectId)
        //{
        //    TaskId = ScheduleManager.TaskId<>() taskObjectId.ToString(); 
        //}

        private bool _isRunning;

        internal DateTime DateTrigger { get; set; }

        internal string TaskId { get; private set; }
       
        //internal abstract ScheduleJobType JobType { get; }

        public abstract TimeSpan ScheduleTo { get; }

        protected abstract void OnStart();

        internal virtual bool ShouldLogSchedule
        {
            get { return true; }
        }

        public void Start()
        {
            _isRunning = true;

            var s = new Stopwatch();
            s.Start();

            try
            {
                //запускаем службу в отдельном потоке
                Task.Factory.StartNew(OnStart).ContinueWith(t =>
                {
                    s.Stop();
                    _isRunning = false;

                    if (t.Exception != null)
                    {
                        //GeniumLogger.Error(t.Exception);
                        Console.WriteLine("Job exception " + t.Exception.Message);
                        //move exception process to schedule manager
                    }
                });
            }
            catch (Exception ex)
            {
                //GeniumLogger.Error(ex);
                Console.WriteLine("Job exception " + ex.Message);
            }
        }

        //public static string GenerateTaskId(T type, string id)
        //{
        //    return $"{type}_{id}";
        //}

        //public static string GenerateTaskId(T type, int id)
        //{
        //    return $"{type}_{id}";
        //}


        //public override string ToString()
        //{
        //    return $"{DateTrigger}: {JobType} ({TaskId})";
        //}

        protected string GetInfo()
        {
            return $"{DateTrigger}: {TaskId}";
        }
    }
}
