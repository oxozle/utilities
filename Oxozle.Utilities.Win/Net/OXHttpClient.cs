﻿#region Unigs

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

#endregion

namespace Oxozle.Utilities.Win.Net
{
    /// <summary>
    ///   Поддерживаемая кодировка
    /// </summary>
    public enum SuppostEncoding
    {
        UTF8,

        Windows1251,

        KOI8R
    }

    public enum RequestType
    {
        GET,
        POST
    }

    public static class HttpClientHelpers
    {
        private static readonly Encoding UTF8 = Encoding.GetEncoding("utf-8");
        private static readonly Encoding Windows1251 = Encoding.GetEncoding("windows-1251");
        private static readonly Encoding Koi8R = Encoding.GetEncoding("koi8-r");

        /// <summary>
        ///   Кодировка
        /// </summary>
        public static Encoding En(this SuppostEncoding encoding)
        {
            switch (encoding)
            {
                case SuppostEncoding.UTF8:
                    return UTF8;
                case SuppostEncoding.Windows1251:
                    return Windows1251;
                case SuppostEncoding.KOI8R:
                    return Koi8R;
                default:
                    return UTF8;
            }
        }
    }


    public class OXHttpClient
    {
        /// <summary>
        ///   User Agent
        /// </summary>
        private const string UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.111 Safari/537.36 OPR/34.0.2036.50";

        private readonly CookieContainer _cookieContainer;
        private readonly Dictionary<string, string> _nextRequestHeaders;
        private string _referrer;

        public OXHttpClient()
        {
            _nextRequestHeaders = new Dictionary<string, string>();
            _cookieContainer = new CookieContainer();
            Proxy = null;

            ServicePointManager.Expect100Continue = false;
        }

        public WebProxy Proxy { get; set; }
        public int TimeoutSecs { get; set; }


        public Exception LastException { get; private set; }

        public void AddHeader(string key, string value)
        {
            _nextRequestHeaders.Add(key, value);
        }

        /// <summary>
        ///   GET запрос
        /// </summary>
        public string Request(string url, SuppostEncoding encoding = SuppostEncoding.UTF8)
        {
            return Request(url, encoding, RequestType.GET, null);
        }

        public string Request(string url, SuppostEncoding encoding, RequestType requestType, string data, string contentType = "application/x-www-form-urlencoded", bool allowAutoRedirect = false)
        {
            if (string.IsNullOrEmpty(_referrer))
                _referrer = url;

            string result = string.Empty;
            string type = (requestType == RequestType.GET) ? "GET" : "POST";

            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = type;
                if (Proxy != null)
                    request.Proxy = Proxy;
                request.AllowAutoRedirect = allowAutoRedirect;
                request.UserAgent = UserAgent;
                request.ContentType = contentType + "; charset=" + encoding.En().HeaderName;
                request.CookieContainer = _cookieContainer;
                request.Referer = Uri.EscapeUriString(_referrer);
                request.Headers[HttpRequestHeader.AcceptEncoding] = "gzip, deflate";
                request.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;
                if (TimeoutSecs > 0)
                    request.Timeout = TimeoutSecs * 1000;

                foreach (KeyValuePair<string, string> nextRequestHeader in _nextRequestHeaders)
                {
                    request.Headers.Add(nextRequestHeader.Key, nextRequestHeader.Value);
                }
                _nextRequestHeaders.Clear();


                if (requestType == RequestType.POST)
                {
                    if (string.IsNullOrEmpty(data))
                        throw new NullReferenceException("post data cant' be null");

                    byte[] buffer = encoding.En().GetBytes(data);
                    request.ContentLength = buffer.Length;

                    Stream stream = request.GetRequestStream();
                    stream.Write(buffer, 0, buffer.Length);
                    stream.Close();
                }

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                if (response.Headers["Set-Cookie"] != null)
                {
                    // Можно сохранить cookie отдельно
                    // _cookies = GetCookies(response.Headers["Set-Cookie"], request.RequestUri.Host.ToString());
                }

                Stream recieveStream = response.GetResponseStream();
                StreamReader sr = new StreamReader(recieveStream, encoding.En());
                result = sr.ReadToEnd();
                sr.Close();
                response.Close();

                _referrer = url;
            }
            catch (Exception exception)
            {
                Console.WriteLine("Error");
                LastException = exception;
                //Debugger.Break();
            }

            return result;
        }

        public string PostService(string url, string data)
        {
            return Request(url, SuppostEncoding.UTF8, RequestType.POST, data);
        }

        public string GetService(string url)
        {
            return Request(url, SuppostEncoding.UTF8);
        }

        public void DownloadFile(string url, string localFileName)
        {
            if (string.IsNullOrEmpty(_referrer))
                _referrer = url;

            string result = string.Empty;
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "GET";
                request.AllowAutoRedirect = false;
                request.UserAgent = UserAgent;
                request.Referer = url;
                request.CookieContainer = _cookieContainer;
                request.Referer = _referrer;
                request.Headers[HttpRequestHeader.AcceptEncoding] = "gzip, deflate";
                request.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;


                HttpWebResponse response = (HttpWebResponse)request.GetResponse();


                // Open the response stream
                using (Stream MyResponseStream = response.GetResponseStream())
                {
                    // Open the destination file
                    using (
                        FileStream MyFileStream = new FileStream(localFileName, FileMode.OpenOrCreate, FileAccess.Write)
                        )
                    {
                        // Create a 4K buffer to chunk the file
                        byte[] MyBuffer = new byte[4096];
                        int BytesRead;
                        // Read the chunk of the web response into the buffer
                        while (0 < (BytesRead = MyResponseStream.Read(MyBuffer, 0, MyBuffer.Length)))
                        {
                            // Write the chunk from the buffer to the file
                            MyFileStream.Write(MyBuffer, 0, BytesRead);
                        }
                    }
                }

                _referrer = url;
            }
            catch (Exception exception)
            {
#if DEBUG
                Console.WriteLine("Error");
                //Debugger.Break();
#endif
            }
        }
    }
}