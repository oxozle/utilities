﻿namespace Oxozle.Utilities.Win.AsyncPool
{
    /// <summary>
    /// Async work result
    /// </summary>
    public class WorkResponse
    {
        public WorkResponse()
        {
        }

        public WorkResponse(object response)
        {
            WorkResult = response;
        }

        /// <summary>
        /// Data
        /// </summary>
        public object WorkResult { get; set; }

        public T GetValue<T>()
        {
            return (T) WorkResult;
        }
    }
}