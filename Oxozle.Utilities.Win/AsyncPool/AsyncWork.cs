﻿#region Unigs

using System;

#endregion

namespace Oxozle.Utilities.Win.AsyncPool
{
    /// <summary>
    /// Async work object
    /// </summary>
    public class AsyntWork
    {
        /// <summary>
        /// Callback when work completedработы)
        /// </summary>
        public Action<WorkResponse> Callback;

        /// <summary>
        /// Callback for UI update
        /// </summary>
        public Action<UIWorkRequest> UpdateStartWork;

        /// <summary>
        /// Async work method
        /// </summary>
        public Func<WorkRequest, WorkResponse> Work;

        /// <summary>
        /// Async work Input Data
        /// </summary>
        public WorkResponse WorkResponse { get; set; }

        /// <summary>
        /// Async work Output Data
        /// </summary>
        public WorkRequest WorkRequest { get; set; }
    }
}