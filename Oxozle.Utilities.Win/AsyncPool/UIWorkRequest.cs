﻿namespace Oxozle.Utilities.Win.AsyncPool
{
    /// <summary>
    /// UI update
    /// </summary>
    public class UIWorkRequest
    {
        /// <summary>
        /// Work title
        /// </summary>
        public string UIWorkName { get; set; }

        /// <summary>
        /// Work description
        /// </summary>
        public string UIWorkDescription { get; set; }

        /// <summary>
        /// Progress percent
        /// </summary>
        public int UIProgressValue { get; set; }
    }
}