﻿// *********************************************************************************
// Author:	Azarov Dmitriy
// Email:	oxozle@gmail.com
// Date: 	30.06.2014
// Project:	Oxozle.Utilities.Win.ImageExtensions.cs
//  
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// *********************************************************************************

#region Usings

using System;
using System.Drawing;
using System.Drawing.Imaging;

#endregion

namespace Oxozle.Utilities.Win.Drawing
{
    public static class ImageExtensions
    {
        public static void SaveJPEG(this Image image, string path, int quality)
        {
            if (quality < 0 || quality > 100)
                throw new ArgumentOutOfRangeException("quality must be between 0 and 100.");

            // Encoder parameter for image quality 
            EncoderParameter qualityParam = new EncoderParameter(Encoder.Quality, quality);
            // Jpeg image codec 
            ImageCodecInfo jpegCodec = GetEncoderInfo("image/jpeg");

            EncoderParameters encoderParams = new EncoderParameters(1);
            encoderParams.Param[0] = qualityParam;


            image.Save(path, jpegCodec, encoderParams);
        }

        /// <summary>
        ///   Returns the image codec with the given mime type
        /// </summary>
        private static ImageCodecInfo GetEncoderInfo(string mimeType)
        {
            // Get image codecs for all image formats 
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();

            // Find the correct image codec 
            for (int i = 0; i < codecs.Length; i++)
                if (codecs[i].MimeType == mimeType)
                    return codecs[i];
            return null;
        }

        public static Image WatermarkImage(this Image source, string watermarkSourceFile)
        {
            Bitmap watermarkBMP = (Bitmap) Image.FromFile(watermarkSourceFile);
            Bitmap watermark = (Bitmap) watermarkBMP.Clone();

            int fullWidth = watermarkBMP.Width + 20;
            int fullHeigh = watermarkBMP.Height + 20;

            int HeightPercent = 15;
            int maxWatermarkHeight = (int) (source.Height*HeightPercent/100.0);


            if (fullWidth > source.Width)
            {
                fullHeigh = fullHeigh*source.Width/fullWidth;
                fullWidth = source.Width - 20;
                Image imgResize = watermarkBMP;
                imgResize = imgResize.GetThumbnailImage(fullWidth, fullHeigh, null, IntPtr.Zero);
                watermark = (Bitmap) imgResize;
            }

            if (fullHeigh > maxWatermarkHeight)
            {
                fullWidth = watermark.Width*maxWatermarkHeight/fullHeigh;
                fullHeigh = maxWatermarkHeight;
                Image imgResize = watermarkBMP;
                imgResize = imgResize.GetThumbnailImage(fullWidth, fullHeigh, null, IntPtr.Zero);
                watermark = (Bitmap) imgResize;
            }


            Graphics graphics = Graphics.FromImage(source);


            int left = source.Width/2 - watermark.Width/2;


            graphics.DrawImage(watermark, new Rectangle(
                left, source.Height - (int) (watermark.Height + 0.05*source.Height),
                watermark.Width,
                watermark.Height
                ));

            graphics.Dispose();
            return source;
        }
    }
}