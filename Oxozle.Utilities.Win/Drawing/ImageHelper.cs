﻿// *********************************************************************************
// Author:	Azarov Dmitriy
// Email:	oxozle@gmail.com
// Date: 	30.06.2014
// Project:	Oxozle.Utilities.Win.ImageHelper.cs
//  
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// *********************************************************************************

#region Usings

using System;
using System.Drawing;
using System.Drawing.Drawing2D;

#endregion

namespace Oxozle.Utilities.Win.Drawing
{
    /// <summary>
    ///   Новая версия файла для работы с изображениями
    /// </summary>
    public class ImageHelper : IDisposable
    {
        /// <summary>
        ///   Расширение в котором сохраняются изображения
        /// </summary>
        public const string ImageExtension = ".jpg";


        private Image _source;

        public ImageHelper(string source)
        {
            _source = Image.FromFile(source);
        }

        /// <summary>
        ///   Объект изображения
        /// </summary>
        public Image ImageData
        {
            get { return _source; }
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (_source != null)
                _source.Dispose();
        }

        #endregion

        /// <summary>
        ///   Уменьшенное изображение
        /// </summary>
        /// <param name="width"> Ширина </param>
        public Image Thumbnail(int width)
        {
            if (_source.Width <= width)
                width = _source.Width;

            int newHeight = _source.Height*width/_source.Width;


            Bitmap newImage = new Bitmap(width, newHeight);
            using (Graphics gr = Graphics.FromImage(newImage))
            {
                gr.SmoothingMode = SmoothingMode.AntiAlias;
                gr.InterpolationMode = InterpolationMode.HighQualityBicubic;
                gr.PixelOffsetMode = PixelOffsetMode.HighQuality;
                gr.DrawImage(_source, new Rectangle(0, 0, width, newHeight));
            }


            return newImage; //_source.GetThumbnailImage(width, newHeight, null, IntPtr.Zero);
        }


        /// <summary>
        ///   Изменяет оригинал изображения
        /// </summary>
        /// <param name="width"> Ширина </param>
        public Image ThumbnailMe(int width)
        {
            _source = Thumbnail(width);
            return _source;
        }

        public Image WatermarkImage(string sourceBitmap)
        {
            using (Image image = Image.FromFile(sourceBitmap))
            {
                return WatermarkImage(image);
            }
        }

        public Image WatermarkImage(Image bitmapWithWatermark)
        {
            //Image watermark = (Image)_source.Clone();

            int fullWidth = bitmapWithWatermark.Width + 20;
            int fullHeigh = bitmapWithWatermark.Height + 20;

            int HeightPercent = 15;
            int maxWatermarkHeight = (int) (_source.Height*HeightPercent/100.0);


            if (fullHeigh > maxWatermarkHeight)
            {
                fullWidth = bitmapWithWatermark.Width*maxWatermarkHeight/fullHeigh;
                fullHeigh = maxWatermarkHeight;
                Image imgResize = bitmapWithWatermark;
                imgResize = imgResize.GetThumbnailImage(fullWidth, fullHeigh, null, IntPtr.Zero);
                bitmapWithWatermark = imgResize;
            }


            using (Graphics graphics = Graphics.FromImage(_source))
            {
                int left = _source.Width/2 - bitmapWithWatermark.Width/2;


                graphics.DrawImage(bitmapWithWatermark, new Rectangle(
                    left,
                    _source.Height -
                    (int) (bitmapWithWatermark.Height + 0.05*_source.Height),
                    bitmapWithWatermark.Width,
                    bitmapWithWatermark.Height
                    ));
            }

            return _source;
        }
    }
}